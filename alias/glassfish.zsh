alias startGlassfish='docker run -p 8080:8080 -p 4848:4848 -p 8181:8181 --name glassfish glassfish:5.0'
alias startGlassfish4='docker run -p 8080:8080 -p 4848:4848 -p 8181:8181 --name glassfish glassfish:4.1.2'
alias openGlassfish='docker exec -it glassfish /bin/bash'
alias stopGlassfish='docker rm $(docker stop glassfish)'
