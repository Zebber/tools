alias startdb='cd /workspace/postgres; docker container run --rm -ti -p 5432:5432 -v $(pwd):/docker-entrypoint-initdb.d/ --name postgres postgres:9.5.9'
alias startSonar='docker run -d --restart always --name sonarqube -p 9000:9000 sonarqube'
alias nginx='docker run --name nginx --rm -v $(pwd):/usr/share/nginx/html:ro -p 8888:80 nginx'
alias jenkins='docker run --rm -u root -v jenkins-data:/var/jenkins_home -v /workspace/:/workspace -v /var/run/docker.sock:/var/run/docker.sock -v "$HOME":/home -p 8080:8080   jenkinsci/blueocean'